package com.zuitt.example;

import java.util.Scanner;

public class S3A1 {
    public static void  main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = 1;
        int factorial = 1;
        System.out.println("Input an integer whose factorial will be computed: ");
        try{
            num = in.nextInt();
            if(num > 0) {
                int x = num;
                while (x >= 1) {
                    factorial = x * factorial;
                    x--;
                }
            } else {
                num = 1;
                System.out.println("The integer should not be negative or 0");
            }
        }
        catch (Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }
        finally {
            System.out.println("The factorial of " + num + " is " + factorial);
        }
    }
}
